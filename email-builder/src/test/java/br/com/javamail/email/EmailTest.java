package br.com.javamail.email;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.CharEncoding;
import org.junit.Test;

import br.com.javamail.email.provider.AuthenticatedSessionProvider;
import br.com.javamail.email.provider.SessionProvider;

import com.google.common.collect.Lists;


public class EmailTest {

	@Test
	public void emailAuthenticatedTest() throws IOException {
		SessionProvider session = AuthenticatedSessionProvider.host("smtp.gmail.com",465).tlsAuth("vieiraviudes@gmail.com", "senha");
		List<File> list = Lists.newArrayList();
		list.add(new File("src/test/resources/459360XXXXXX2772.pdf"));
		EmailBody body = EmailBody.builder()
				.attachments(list)
		        .content("src/test/resources/test.html",CharEncoding.UTF_8,"html")
		        .build();

		try {
			Email.session(session)
			        .from(new InternetAddress("vieiraviudes@gmail.com"))
			        .to(new InternetAddress("andersonvieiraviudes@gmail.com"))
			        .subject("TEstes de Email")
			        .body(body)
			        .build()
			        .send();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

}
