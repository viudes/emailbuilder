package br.com.javamail.email.builder;

import java.io.File;
import java.io.IOException;
import java.util.List;

import br.com.javamail.email.EmailBody;

import com.google.common.collect.Table;
import com.google.common.net.MediaType;


/**
 * Defines a contract for implementations that can create basic email bodies.
 */
/**
 * @author Indra
 *
 */
/**
 * @author Indra
 *
 */
public interface EmailBodyBuilder {

	/**
	 * Sets the content for the body.
	 * 
	 * @param body
	 *           The body.
	 * @return The builder.
	 */
	public EmailBodyBuilder content(String body);

	/**
	 * Sets the type for the body.
	 * 
	 * @param type
	 *           The type.
	 * @return The builder.
	 */
	public EmailBodyBuilder type(String type);

	/**
	 * Sets content for the target tag.
	 * 
	 * @param tag
	 *           The tag.
	 * @param content
	 *           The content.
	 * @return The builder.
	 */
	public EmailBodyBuilder replace(String tag, Object content);

	/**
	 * Sets the delimiter for templates.
	 * 
	 * @param delimiter
	 *           The delimiter.
	 * @return The builder.
	 */
	public EmailBodyBuilder delimiter(char delimiter);

	/**
	 * Creates an EmailBody.
	 * 
	 * @return A new EmailBody instance.
	 */
	public EmailBody build();

	
	/**
	 *  Creates attachments in body email
	 * 
	 * @param files
	 * @return the builder
	 */
	public EmailBodyBuilder attachments(List<File> files);
	
	/**
	 *  Creates attachments in body email
	 * 
	 * @param files  
	 * @return the builder
	 */
	public EmailBodyBuilder attachments(Table<MediaType, String, byte[]> files);

	
	
	/**
	 * 
	 * Sets the content for the body. 
	 * 
	 * @param file of text
	 * @param encode type
	 * @param type of MINEType
	 * @return the builder
	 * @throws IOException
	 */
	public EmailBodyBuilder content(String file, String encode, String type) throws IOException;
	
	/**
	 * Sets the content for the body.
	 * 
	 * @param body
	 * @param encode
	 * @return
	 */
	public EmailBodyBuilder content(String body, String encode);
}
