package br.com.javamail.email.builder;

/**
 * Defines a contract for a basic Authenticated SessionProvider builder. 
 */
public interface AuthenticatedSessionProviderBuilder extends SessionProviderBuilder {
    /**
     * Gets the username.
     * 
     * @return The username.
     */
    public String getUsername();

    /**
     * Gets the password.
     * 
     * @return The password.
     */
    public String getPassword();
}
