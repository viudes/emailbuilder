package br.com.javamail.email;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;

import java.io.File;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.PreencodedMimeBodyPart;
import javax.mail.util.ByteArrayDataSource;

import br.com.javamail.email.builder.EmailBuilder;
import br.com.javamail.email.provider.SessionProvider;

import com.google.common.collect.Table.Cell;
import com.google.common.net.MediaType;

/**
 * Abstract object representing an email.
 */
public final class Email {
	private final InternetAddress fromAddress;
	private final List<InternetAddress> toAddresses;
	private final List<InternetAddress> ccAddresses;
	private final List<InternetAddress> bccAddresses;
	private final String subject;
	private final EmailBody body;
	private final SessionProvider sessionProvider;

	/**
	 * Initializes a new instance of the Email class.
	 * 
	 * @param builder
	 *           The email builder to initialize with.
	 */
	protected Email(Builder builder) {

		// Set fields.
		this.fromAddress = builder.fromAddress;
		this.toAddresses = builder.toAddresses;
		this.ccAddresses = builder.ccAddresses;
		this.bccAddresses = builder.bccAddresses;
		this.subject = builder.subject;
		this.body = builder.body;
		this.sessionProvider = builder.sessionProvider;
	}

	/**
	 * Creates a builder with the required session information.
	 * 
	 * @param provider
	 *           The session provider.
	 * @return An EmailBuilder.
	 */
	public static EmailBuilder session(SessionProvider provider) {
		return new Builder(provider);
	}

	/**
	 * Gets the from address.
	 * 
	 * @return The from address.
	 */
	public InternetAddress getFromAddress() {
		return this.fromAddress;
	}

	/**
	 * Gets the TO address list. Unmodifiable.
	 * 
	 * @return An unmodifiable list of TO addresses.
	 */
	public List<InternetAddress> getToAddresses() {
		return java.util.Collections.unmodifiableList(this.toAddresses);
	}

	/**
	 * Gets the CC address list. Unmodifiable.
	 * 
	 * @return An unmodifiable list of CC addresses.
	 */
	public List<InternetAddress> getCcAddresses() {
		return java.util.Collections.unmodifiableList(this.ccAddresses);
	}

	/**
	 * Gets the BCC address list. Unmodifiable.
	 * 
	 * @return An unmodifiable list of BCC addresses.
	 */
	public List<InternetAddress> getBccAddresses() {
		return java.util.Collections.unmodifiableList(this.bccAddresses);
	}

	/**
	 * Gets the subject line.
	 * 
	 * @return The subject line.
	 */
	public String getSubject() {
		return this.subject;
	}

	/**
	 * Gets the email body.
	 * 
	 * @return The email body.
	 */
	public EmailBody getBody() {
		return this.body;
	}

	/**
	 * Sends the email.
	 * 
	 * @throws MessagingException
	 */
	public void send() throws MessagingException {

		MimeMessage message = new MimeMessage(this.sessionProvider.getSession());
		message.setSubject(this.getSubject());
		// Set from address.
		message.setFrom(this.getFromAddress());
		// Set TO addresses.
		if (isNotEmpty(this.getToAddresses())) {
			message.addRecipients(Message.RecipientType.TO, InternetAddress.toString(this.getToAddresses().toArray(new InternetAddress[this.getToAddresses().size()])));
		}

		// Set CC addresses
		if (isNotEmpty(this.getCcAddresses())) {
			message.addRecipients(Message.RecipientType.CC, InternetAddress.toString(this.getCcAddresses().toArray(new InternetAddress[this.getCcAddresses().size()])));
		}

		// Set BCC addresses
		if (isNotEmpty(this.getBccAddresses())) {
			message.addRecipients(Message.RecipientType.BCC, InternetAddress.toString(this.getBccAddresses().toArray(new InternetAddress[this.getBccAddresses().size()])));
		}

		if (this.getBody().getFiles() != null) {
			Multipart mp = new MimeMultipart();
			PreencodedMimeBodyPart corpoEmail = new PreencodedMimeBodyPart("base64");
			corpoEmail.setText(this.getBody().getContent(),this.getBody().getEncode(),this.getBody().getType().replace("text/", ""));
			mp.addBodyPart(corpoEmail);
			if (isNotEmpty(this.getBody().getFiles().getFilesFile())) {
				addAttachmentFile(this.getBody().getFiles(), mp);
			} else if (this.getBody().getFiles().getFilesByte().size()>0) {
				addAttachmentFileByteArray(this.getBody().getFiles(), mp);
			}
			message.setContent(mp);
		} else {
			message.setText(this.getBody().getContent(),this.getBody().getEncode(),this.getBody().getType().replace("text/", ""));
		}
		message.saveChanges();
		Transport.send(message);
	}

	protected void addAttachmentFile(Attachment attachment, Multipart mp) throws MessagingException {
		for (File file : attachment.getFilesFile()) {
			MimeBodyPart mbp2 = new MimeBodyPart();
			mbp2.setDisposition(Part.ATTACHMENT);
			FileDataSource fds = new FileDataSource(file);
			mbp2.setDataHandler(new DataHandler(fds));
			mbp2.setFileName(fds.getName());
			mbp2.setHeader("Content-ID", file.getName());
			mp.addBodyPart(mbp2);
		}
	}

	protected void addAttachmentFileByteArray(Attachment attachment, Multipart mp) throws MessagingException {
		for (Cell<MediaType, String, byte[]> entry : attachment.getFilesByte().cellSet()) {
			String arquivo = entry.getColumnKey();
			MediaType mediaType = entry.getRowKey();
			byte[] data = entry.getValue();
			MimeBodyPart mbp2 = new MimeBodyPart();
			mbp2.setDisposition(Part.ATTACHMENT);
			DataSource fds = new ByteArrayDataSource(data, mediaType.toString());
			mbp2.setDataHandler(new DataHandler(fds));
			mbp2.setFileName(arquivo);
			mbp2.setHeader("Content-ID", arquivo);
			mp.addBodyPart(mbp2);
		}
	}

	/**
	 * Builds email objects. I opted for an interface and a default
	 * implementation here instead of concrete implementations and redirection
	 * trickery. It's just easier to understand in my opinion. You can still
	 * subclass this and override it's behavior if needed, but it should do
	 * pretty much everything you'd need.
	 */
	public final static class Builder implements EmailBuilder {
		private SessionProvider sessionProvider;
		private InternetAddress fromAddress;
		private List<InternetAddress> toAddresses;
		private List<InternetAddress> ccAddresses;
		private List<InternetAddress> bccAddresses;
		private String subject;
		private EmailBody body;

		/**
		 * Initializes a new instance of the Email.Builder class.
		 * 
		 * @param sessionProvider
		 *           The session provider.
		 */
		public Builder(SessionProvider sessionProvider) {
			checkArgument(sessionProvider != null, "Session Provider cannot be null!");

			this.fromAddress = null;
			this.toAddresses = newArrayList();
			this.ccAddresses = newArrayList();
			this.bccAddresses = newArrayList();
			this.subject = "";
			this.body = EmailBody.fromString("");
			this.sessionProvider = sessionProvider;
		}

		@Override
		public EmailBuilder to(InternetAddress... toAddress) {
			this.toAddresses.addAll(newArrayList(toAddress));
			return this;
		}

		@Override
		public EmailBuilder from(InternetAddress fromAddress) {
			this.fromAddress = fromAddress;
			return this;
		}

		@Override
		public EmailBuilder cc(InternetAddress... ccAddress) {
			this.ccAddresses.addAll(newArrayList(ccAddress));
			return this;
		}

		@Override
		public EmailBuilder bcc(InternetAddress... bccAddress) {
			checkArgument(isNotEmpty(bccAddresses), "BCC address cannot be null!");
			this.bccAddresses.addAll(newArrayList(bccAddress));
			return this;
		}

		@Override
		public EmailBuilder subject(String subject) {
			this.subject = subject;
			return this;
		}

		@Override
		public EmailBuilder body(EmailBody body) {
			checkArgument(body != null, "Body cannot be null!");
			this.body = body;
			return this;
		}

		@Override
		public Email build() {
			checkState(this.fromAddress != null, "From address cannot be null or empty!");
			checkArgument(isNotEmpty(this.toAddresses), "At least one TO: address is required!");
			return new Email(this);
		}
	}
}
