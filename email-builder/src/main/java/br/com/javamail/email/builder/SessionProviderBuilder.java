package br.com.javamail.email.builder;

/**
 * Defines a contract for a basic SessionProvider builder. 
 */
public interface SessionProviderBuilder {
    /**
     * Gets the host.
     * 
     * @return The host.
     */
    public String getSmtpHost();

    /**
     * Gets the port.
     * 
     * @return The port.
     */
    public int getPort();

    /**
     * Gets the Debug mode.
     * 
     * @return The port.
     */
	public boolean getDebug();
}
