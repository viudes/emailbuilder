package br.com.javamail.email;

import java.io.File;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.net.MediaType;

/**
 * 
 * 
 * @author Indra
 *
 */
public class Attachment {

	private List<File> filesFile = Collections.emptyList();
	private Table<MediaType,String, byte[]> filesByte = HashBasedTable.create();

	public List<File> getFilesFile() {
		return filesFile;
	}

	public void setFilesFile(List<File> filesFile) {
		this.filesFile = filesFile;
	}

	public Table<MediaType, String, byte[]> getFilesByte() {
		return filesByte;
	}

	public void setFilesByte(Table<MediaType, String, byte[]> filesByte) {
		this.filesByte = filesByte;
	}
	
	
}
